#!/usr/bin/python3
import argparse
import glob
import math
import itertools

# try to find faster JSON libraries
try:
    import ujson as json
except ImportError:
    try:
        import rapidjson as json  # type: ignore
    except ImportError:
        import json  # type: ignore
import logging
import multiprocessing.pool
from pathlib import Path
import re
import sys
from typing import Any, Dict, Iterable, List, Set, Tuple

from cycler import cycler
import matplotlib

# pylint: disable=wrong-import-order,wrong-import-position
matplotlib.use("Agg")
import matplotlib.colors as mcolors
import matplotlib.pyplot as plt

sys.path.insert(0, str(Path(__file__).resolve().parent))
from resmon import constants, config, util

# pylint: enable=wrong-import-order,wrong-import-position


# maximum permissible variation in sampling period length
PERIOD_PRECISION_DEC = 2  # decimal places
PERIOD_PRECISION_VAL = 1 / (10**PERIOD_PRECISION_DEC)  # seconds

LINE_STYLES = matplotlib.cbook.ls_mapper.values()


class LineStyleAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        try:
            regex = re.compile(values[0])
        except re.error as e:
            raise argparse.ArgumentError(
                self, f"first linestyle argument is not a regex: {e}"
            )
        style = values[1]
        if style not in LINE_STYLES:
            raise argparse.ArgumentError(
                self,
                f"second linestyle argument must be one of: {', '.join(LINE_STYLES)}",
            )
        linestyles = getattr(namespace, self.dest) or {}
        linestyles[regex] = style
        setattr(namespace, self.dest, linestyles)


class DirectoryAction(argparse.Action):
    def validate_directory(self, dirname):
        dirpath = Path(dirname)
        if (
            not dirpath.is_dir()
            or not (dirpath / constants.RESMON_PARSED_DIRECTORY).is_dir()
        ):
            raise argparse.ArgumentError(
                argument=self,
                message=(
                    f"missing directory {dirpath}/{constants.RESMON_PARSED_DIRECTORY}"
                ),
            )

    def __call__(self, parser, namespace, values, option_string=None):
        assert isinstance(values, list)
        for dirname in values:
            self.validate_directory(dirname)
        setattr(namespace, self.dest, values)


class NamedGroupAction(DirectoryAction):
    def __call__(self, parser, namespace, values, option_string=None):
        if not isinstance(values, list) or len(values) <= 1:
            raise argparse.ArgumentError(
                self,
                "name required at first position, followed by one or more paths "
                "to directories",
            )
        groups = getattr(namespace, self.dest) or {}
        group_name = values[0]
        groups[group_name] = values[1:]
        for dirname in groups[group_name]:
            self.validate_directory(dirname)
        setattr(namespace, self.dest, groups)


def chart_init_plot(chart_config: Dict[str, Any], y_prefix: str, period: float):
    figsize = chart_config.get("figsize", (16, 9))
    xlabel = chart_config.get("xlabel", "Time [s]")
    ylabel = chart_config.get("ylabel", "")
    unit = chart_config.get("unit", "")

    fig, ax = plt.subplots(figsize=figsize)

    ax.set_xlabel(xlabel)
    ytext = ylabel
    if unit:
        ytext += f" [{y_prefix}{unit}"
    if period:
        if period == 1:
            ytext += "/second"
        else:
            ytext += f"/{period} seconds"
    if unit:
        ytext += "]"
    if period:
        ytext += " - value increments"
    if ytext:
        ax.set_ylabel(ytext.strip())
    ax.set_title(chart_config["title"])

    ax.grid(True, axis="x", which="both", linestyle="dotted")
    ax.grid(True, axis="y", which="both", linestyle="dotted")

    colors = list(mcolors.TABLEAU_COLORS.keys()) + list(mcolors.BASE_COLORS.keys())
    default_cycler = cycler(color=colors)

    plt.rc("axes", prop_cycle=default_cycler)

    plt.minorticks_on()

    return fig, ax


def num_to_unit(val, multiplier):
    names = ["k", "M", "G", "T"]
    if multiplier == 1024:
        names = [f"{name}i" for name in names]
    elif multiplier != 1000:
        raise NotImplementedError("scalebase != 1000 or 1024")
    unit = 1
    name = ""
    for power, candidate_name in enumerate(names, start=1):
        candidate_unit = multiplier**power
        if val >= candidate_unit:
            unit = candidate_unit
            name = candidate_name
        else:
            break
    return unit, name


def stats2xy(stats, time_zero, until_relative, avg_interval, relative_values: bool):
    assert len(stats) >= 2
    it = iter(stats)
    first_time_from, first_time_to, _value = next(it)
    if first_time_from == first_time_to:
        return stats2xy_point_in_time(stats, time_zero, until_relative, avg_interval)

    period = round(first_time_to - first_time_from, PERIOD_PRECISION_DEC)
    return stats2xy_rate(
        stats, time_zero, until_relative, avg_interval, period, relative_values
    )


def stats2xy_point_in_time(stats, time_zero, until_relative, avg_interval):
    xvalues = []
    yvalues = []
    if not avg_interval:
        for _, time_s, value in stats:
            reltime = time_s - time_zero
            if 0 < reltime <= until_relative:
                xvalues.append(reltime)
                yvalues.append(value)
    else:
        cur_interval_start = None
        sum_values = []
        for time_from, time_to, value in stats:
            if cur_interval_start is None:
                cur_interval_start = time_from
            if time_to - cur_interval_start < avg_interval:
                sum_values.append(value)
            else:
                xvalues.append(
                    cur_interval_start + (time_to - cur_interval_start) / 2 - time_zero
                )
                yvalues.append(math.fsum(sum_values) / len(sum_values))
                cur_interval_start = None
                sum_values = []
        if len(sum_values) != 0:  # last point
            time_last = stats[-1][1]
            xvalues.append(
                cur_interval_start + (time_last - cur_interval_start) / 2 - time_zero
            )
            yvalues.append(math.fsum(sum_values) / len(sum_values))
    return (xvalues, yvalues, None)


# pylint: disable=too-many-positional-arguments
def stats2xy_rate(
    stats,
    time_zero,
    until_relative,
    avg_interval: float,
    period: float,
    relative_values: bool,
):
    if avg_interval:
        if avg_interval <= period:
            raise ValueError(
                f"averaging interval {avg_interval} cannot be shorter or same as original period {period}"
            )
        avg_batch = int(avg_interval / period)
        if avg_batch != (avg_interval / period):
            raise ValueError(
                f"averaging interval {avg_interval} is not multiple of original period {period}"
            )
    else:
        avg_batch = 1

    xvalues = []
    yvalues = []

    statit = iter(stats)
    while True:
        try:
            batch_start, batch_to, first_value = next(statit)
        except StopIteration:
            break
        batch = [first_value]
        while len(batch) < avg_batch:
            try:
                next_from, next_to, next_value = next(statit)
                assert math.isclose(
                    next_to, next_from, rel_tol=PERIOD_PRECISION_VAL
                ), f"period unstable: delta({next_to}, {next_from}) > {PERIOD_PRECISION_VAL}"
            except StopIteration:
                break
            assert (
                batch_to == next_from
            ), f"discontinuity detected: jump from {batch_to}, {next_from}"
            batch_to = next_to
            batch.append(next_value)
        xval = (batch_to + batch_start) / 2 - time_zero
        if xval > until_relative:
            break
        xvalues.append(xval)
        yval = math.fsum(
            batch
        )  # not an average, but in fact sum because the rate is over longer interval
        if relative_values:
            # ... except for relative values - summation does not make sense for %
            yval /= len(batch)
        yvalues.append(yval)
        batch = []
    return (xvalues, yvalues, period * avg_batch)


def read_shotgun_time_boundaries(directory: Path):
    # quick hack to determine x min/max - pick a random Shotgun JSON
    try:
        shotgun_path = next((directory / "results-shotgun/data").glob("*.json"))
    except StopIteration as ex:
        raise RuntimeError(
            f"missing file {directory}/{constants.SHOTGUN_DATA_DIR}/*.json\n"
            f"use --plot-abs-time to plot charts without Shotgun metadata"
        ) from ex
    with open(shotgun_path, encoding="utf-8") as shotgun_json:
        shotgun_res = json.load(shotgun_json)
    # offset for time 0
    time_zero = shotgun_res["stats_sum"]["since_ms"] / 1000
    time_end = shotgun_res["stats_sum"]["until_ms"] / 1000
    return time_zero, time_end


def chart_load_dirdata(
    dirname: str,
    chart_id: str,
    avg_interval: float,
    abs_time: bool,
    relative_values: bool,
) -> Dict[str, Any]:
    if abs_time:
        time_zero = 0
        time_end = float("+inf")
    else:
        time_zero, time_end = read_shotgun_time_boundaries(Path(dirname))
    fname = Path(dirname) / constants.RESMON_PARSED_DIRECTORY / f"{chart_id}.json"
    if not fname.is_file():
        raise NotImplementedError  # TODO handle missing statfile
    logging.debug("loading %s", fname)
    with open(fname, "r", encoding="utf-8") as statfile:
        stats = json.load(statfile)

    for cgrp in stats:
        for stat, values in stats[cgrp].items():
            assert chart_id == util.cgroup_stat_to_chart_id(
                cgrp, stat
            ), f"unexpected data in {fname}"
            max_x = time_end - time_zero + constants.PLOT_TRAILING_TIME_S
            xvalues, yvalues, rate = stats2xy(
                values, time_zero, max_x, avg_interval, relative_values
            )
            return {  # each file has only one stat for one cgroup
                "xvalues": xvalues,
                "yvalues": yvalues,
                "rate": rate,
            }
    raise ValueError(f"invalid stat file {fname}")


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description="Plot resource usage")

    parser.add_argument(
        "result_dirs", nargs="*", help="Ansible result dirs", action=DirectoryAction
    )
    conf_action = parser.add_argument(
        "-c",
        "--config",
        type=Path,
        default=constants.RESMON_CONFIG_PATH,
        action=config.ConfigAction,
        help="configuration file",
    )
    parser.add_argument(
        "-g",
        "--group",
        nargs="+",
        action=NamedGroupAction,
        default={},
        help="group_name directory [directory ...]; can be used multiple times",
    )
    parser.add_argument(
        "--average", type=float, help="interval to average over [seconds]"
    )
    parser.add_argument(
        "--prefix", type=str, default="", help="prefix output file names"
    )
    parser.add_argument(
        "--plot-abs-time",
        action="store_true",
        help="plot values with absolute timestamps instead of using Shotgun datafile",
    )
    parser.add_argument("--img-format", default="png", help="output image format")
    parser.add_argument(
        "--linestyle",
        nargs=2,
        action=LineStyleAction,
        default={},
        help=(
            "change style for series with names matching regex; "
            "name_regex linestyle_name (can be specified multiple times)"
        ),
    )

    args = parser.parse_args()
    if not args.group and not args.result_dirs:
        parser.error(
            "at least one input JSON file required (individually or in a group)"
        )
    if args.config == constants.RESMON_CONFIG_PATH:
        # invoke action that parses config manually (it is not applied to default=)
        conf_action(parser, args, conf_action.default, "-c/--config")
    return args


def deltas(initer):
    """Reduce list to sequence of deltas"""
    it = iter(initer)
    try:
        prev = next(it)
    except StopIteration:
        return
    for nextval in it:
        yield nextval - prev
        prev = nextval


def get_sampling_period(xvalues):
    """
    Check if time period between all x values are the same and return it.

    Slight deviance is accounted for by rounding the time intervals to
    specified number of decimal places.
    """

    sampling_intervals = set(
        map(lambda onex: round(onex, PERIOD_PRECISION_DEC), deltas(xvalues))
    )
    if len(sampling_intervals) != 1:
        raise NotImplementedError(
            "expected a single sampling interval but found", sampling_intervals
        )
    return sampling_intervals.pop()


def linestyle_from_name(name, linestyles):
    for name_re, style in linestyles.items():
        if name_re.search(name):
            return style
    return "solid"


def get_all_chart_ids(dirnames: Iterable[str]) -> Set[str]:
    all_chart_ids: Set[str] = set()
    for dirname in dirnames:
        candidates = f"{dirname}/{constants.RESMON_PARSED_DIRECTORY}/*.json"
        for pathname in glob.glob(candidates):
            path = Path(pathname)
            all_chart_ids.add(path.stem)
    return all_chart_ids


def chart_get_max_y(dirdata: Dict[str, Any]) -> float:
    max_y = -math.inf
    for chart_data in dirdata.values():
        chart_max_y = max(chart_data["yvalues"])
        max_y = max(max_y, chart_max_y)
    return max_y


def chart_is_all_zeros(dirdata: Dict[str, Any]) -> bool:
    for chart_data in dirdata.values():
        if any(chart_data["yvalues"]):
            return False
    return True


def chart_get_unit_prefix(chart_config, max_y: float) -> Tuple[float, str]:
    if "scalebase" in chart_config:
        return num_to_unit(max_y, chart_config["scalebase"])
    # no multipliers specified in config, no auto-scaling
    return (1, "")


def chart_plot_envelope(
    ax: Any,
    valuesets: List[Tuple[List[float], List[float]]],
    label: str,
    linestyle: str,
) -> None:
    # compute bounding values
    allxy = sorted(
        itertools.chain.from_iterable(
            zip(x_values, y_values) for x_values, y_values in valuesets
        )
    )
    xstart, ynow = allxy[0]  # initial X value
    yacc = [ynow]
    xstep = 1  # min/max window step
    xbetween = []
    yminbetween = []
    ymaxbetween = []
    yavgbetween = []
    for point in allxy:
        xnow, ynow = point
        if xnow < xstart + xstep:
            yacc.append(ynow)
        else:
            xbetween.append(xstart + xstep / 2)
            yminbetween.append(min(yacc))
            ymaxbetween.append(max(yacc))
            yavgbetween.append(math.fsum(yacc) / len(yacc))
            yacc = [ynow]
            xstart = xnow
    ax.fill_between(xbetween, yminbetween, ymaxbetween, alpha=0.2)
    ax.plot(xbetween, yavgbetween, marker=" ", label=label, linestyle=linestyle)


def chart_plot_group(
    ax: Any,
    chart_data_list: List[Dict[str, Any]],
    name: str,
    prefix_size: float,
    linestyle: str,
) -> None:
    single_run = len(chart_data_list) == 1
    fillbetween: List[Tuple[List[float], List[float]]] = []

    for chart_data in chart_data_list:
        x_vals = chart_data["xvalues"]
        y_vals = [y / prefix_size for y in chart_data["yvalues"]]
        if single_run:
            ax.plot(
                x_vals,
                y_vals,
                label=name,
                marker=" ",
                linestyle=linestyle,
            )
        else:
            fillbetween.append((x_vals, y_vals))

    if not single_run:
        assert len(fillbetween) > 1
        chart_plot_envelope(ax, fillbetween, name, linestyle)


def plot_chart(chart_id: str, args):
    dirdata = {}

    chart_config = config.get_chart_config(args.config, chart_id)
    if not chart_config:  # skip charts which weren't configured to plot
        return

    for groupname, groupdirs in args.group.items():
        for dirname in groupdirs:
            chart_data = chart_load_dirdata(
                dirname,
                chart_id,
                args.average,
                args.plot_abs_time,
                chart_config.get("relative", False),
            )
            dirdata[dirname] = chart_data

    if chart_is_all_zeros(dirdata):
        logging.debug("skipping chart with all zeros: %s", chart_id)
        return

    max_y = chart_get_max_y(dirdata)
    prefix_size, prefix_name = chart_get_unit_prefix(chart_config, max_y)

    rate = next(iter(dirdata.values()))["rate"]
    assert all(
        chart_data["rate"] == rate for r in dirdata.values()
    ), "inconsistent sample rates"
    # do not use %/second, it just looks weird
    plot_rate = not chart_config.get("relative", False) and rate

    fig, ax = chart_init_plot(chart_config, prefix_name, plot_rate)

    for groupname, groupdirs in args.group.items():
        logging.debug("plotting group %s: %s", groupname, chart_id)
        linestyle = linestyle_from_name(groupname, args.linestyle)
        chart_data_list = [dirdata[dirname] for dirname in groupdirs]
        chart_plot_group(ax, chart_data_list, groupname, prefix_size, linestyle)

    if not args.plot_abs_time:
        ax.set_xlim(xmin=0)
    ax.set_ylim(ymin=0)
    ax.legend()
    fig.tight_layout()
    logging.info("saving %s", chart_id)
    fig.savefig(f"{args.prefix}{chart_id}.{args.img_format}")

    # remove references to ax & fig to allow objects to be garbage collected
    plt.close(fig)


def main():
    args = parse_args()
    logging.basicConfig(level=logging.INFO)
    logging.getLogger("matplotlib").setLevel(logging.WARNING)

    # fake groups for individual input directories
    for dirname in args.result_dirs:
        args.group[dirname] = [dirname]

    dirnames = []
    for group_dirs in args.group.values():
        dirnames.extend(group_dirs)

    all_chart_ids = get_all_chart_ids(dirnames)

    with multiprocessing.pool.Pool() as p:
        p.starmap(
            plot_chart,
            [(chart_id, args) for chart_id in all_chart_ids],
            chunksize=1,
        )


if __name__ == "__main__":
    main()
