from pathlib import Path

CGROUP_BASE_DIR = "/sys/fs/cgroup/system.slice"
RESMON_CONFIG_FILENAME = "resmon.yaml"
RESMON_CONFIG_PATH = Path(__file__).resolve().parent.parent / RESMON_CONFIG_FILENAME
RESMON_CAPTURED_FILENAME = "resmon-captured.json"
RESMON_PARSED_DIRECTORY = "resmon-parsed"
SHOTGUN_DATA_DIR = "results-shotgun/data"
PLOT_TRAILING_TIME_S = 10
"""number of seconds that are still plotted on the X-axis after end of data"""
