import argparse
from pathlib import Path
import re
from typing import Any, Dict

import yaml


class ConfigAction(argparse.Action):
    def load_config(self, path: Path):
        try:
            with open(path, "r", encoding="utf-8") as stream:
                try:
                    return yaml.safe_load(stream)
                except yaml.YAMLError as ex:
                    raise argparse.ArgumentError(
                        argument=self, message="invalid config file format"
                    ) from ex
        except FileNotFoundError as ex:
            raise argparse.ArgumentError(
                argument=self, message=f"config file {path} not found"
            ) from ex

    def __call__(self, parser, namespace, values, option_string=None):
        path = values
        config = self.load_config(path)
        setattr(namespace, self.dest, config)


def get_chart_config(config: Dict[str, Any], name: str) -> Dict[str, Any]:
    custom_config = {}  # type: Dict[str, Any]
    for chart_config in config["charts"]:
        m = re.match(chart_config["match"], name)
        if m is not None:
            if custom_config:
                raise ValueError(
                    f"overlaping matches: {custom_config['match']} vs. {chart_config['match']}"
                )
            custom_config = chart_config.copy()
            title = custom_config["title"]
            while ref := re.search(r"(\\([0-9]+))", title):
                try:
                    title = title.replace(ref.group(1), m.group(int(ref.group(2))))
                except IndexError as exc:
                    raise ValueError(
                        "reference not found", title, ref.group(1), ref.group(2)
                    ) from exc
            custom_config["title"] = title
    return custom_config
