from abc import ABC, abstractmethod
import json
import math
import re


class StatParser(ABC):
    def __init__(self, _):
        pass

    @abstractmethod
    def parse(self, record):
        """Stateless parser: Parse whole input text an return data items."""

    @abstractmethod
    def process(self, record):
        """
        Transform one record into zero or more data points in format:
        (metric name, timestamp from, timestamp to, value)
        Point in time metrics: timestamp from == to
        Cumulative metrics: timestamp (from, to]
        For cumulative values, do not yield anything if parser currently
        does not have enough data yet.
        """


class MemoryCurrent(StatParser):
    """parse current memory.current value (point in time), stateless"""

    def parse(self, record):
        return int(record["text"])

    def process(self, record):
        yield ("memory.current", record["ts"], record["ts"], self.parse(record))


class MemoryStat(StatParser):
    """
    parse memory.stat: some keys have point-in-time values, some are cumulative
    see: https://www.kernel.org/doc/html/latest/admin-guide/cgroup-v2.html#memory
    """

    def __init__(self, record):
        self.last_ts = record["ts"]
        self.last_data = DeltaDict(self.parse(record))
        self.keys_with_abs_values = set(
            [  # ordered as in Linux kernel docs
                "anon",
                "file",
                "kernel",
                "kernel_stack",
                "pagetables",
                "sec_pagetables",
                "percpu",
                "sock",
                "vmalloc",
                "shmem",
                "zswap",
                "zswapped",
                "file_mapped",
                "file_dirty",
                "file_writeback",
                "swapcached",
                "anon_thp",
                "file_thp",
                "shmem_thp",
                "inactive_anon",
                "active_anon",
                "inactive_file",
                "active_file",
                "unevictable",
                "slab_reclaimable",
                "slab_unreclaimable",
                "slab",
            ]
        )

    def parse(self, record):
        data = {}
        for line in record["text"].split("\n"):
            if not line:
                continue
            name, textval = line.split(maxsplit=1)
            data[name] = int(textval)
        return data

    def process(self, record):
        now = record["ts"]
        new_data = self.parse(record)
        for key, new_value in new_data.items():
            if key in self.keys_with_abs_values:
                yield (f"memory.{key}", now, now, new_value)
            elif now > self.last_ts:  # cumulative metric
                diff = new_value - self.last_data[key]
                yield (f"memory.{key}", self.last_ts, now, diff)
        self.last_ts = now
        self.last_data = new_data


class SockStat(StatParser):
    """parse socket statistics, stateless"""

    def parse(self, record):
        data = {}  # protocol: metric: value
        for line in record["text"].split("\n"):
            if not line:
                continue
            protocol, metrics_text = line.split(": ")
            metrics = metrics_text.split()
            assert len(metrics) >= 2 and len(metrics) % 2 == 0
            data[protocol] = {}
            for m_idx in range(0, len(metrics), 2):
                metric_name = metrics[m_idx]
                metric_val = int(metrics[m_idx + 1])
                data[protocol][metric_name] = metric_val
        return data

    def process(self, record):
        data = self.parse(record)
        for protocol, metrics in data.items():
            for metric, value in metrics.items():
                yield (
                    f"sockstat.{protocol}.{metric}",
                    record["ts"],
                    record["ts"],
                    value,
                )


class Pressure(StatParser):
    """parse cpu/memory/io.some values; cumulative values"""

    regex = re.compile("^some.*total=([0-9]+)$", flags=re.MULTILINE)
    name = ""

    def __init__(self, record):
        self.last_ts = record["ts"]
        self.last_total = self.parse(record)

    def parse(self, record):
        m = self.regex.search(record["text"])
        assert m, "pressure file format did not match"
        return int(m.group(1))

    def process(self, record):
        now = record["ts"]
        if now == self.last_ts:
            return  # nothing to do, we need another data point

        new_total = self.parse(record)
        percent = (new_total - self.last_total) / (now - self.last_ts) / 1000000 * 100
        yield (f"{self.name}.pressure.some", self.last_ts, now, percent)
        self.last_ts = now
        self.last_total = new_total


class CPUPressure(Pressure):
    name = "cpu"


class IOPressure(Pressure):
    name = "io"


class MemoryPressure(Pressure):
    name = "memory"


class NetworkDevIO(StatParser):
    def __init__(self, record):
        self.last_ts = record["ts"]
        self.last_data = self.parse(record)

    def parse(self, record):
        data = {}  # interface -> tx/rx-stat -> value
        lines = record["text"].split("\n")[2:]
        column_names = (
            "rx.bytes",
            "rx.packets",
            "rx.errs",
            "rx.drop",
            "rx.fifo",
            "rx.frame",
            "rx.compressed",
            "rx.multicast",
            "tx.bytes",
            "tx.packets",
            "tx.errs",
            "tx.drop",
            "tx.fifo",
            "tx.colls",
            "tx.carrier",
            "tx.compressed",
        )
        for line in lines:
            if not line:
                continue
            in_columns = line.split()
            assert in_columns[0][-1] == ":", "unexpected interface name"
            iface_name = in_columns[0][:-1]
            assert len(column_names) == len(in_columns) - 1, "unexpected columns"

            num_columns = list(int(val) for val in in_columns[1:])
            data[iface_name] = dict(zip(column_names, num_columns))
        return data

    def process(self, record):
        now = record["ts"]
        if now == self.last_ts:
            return  # nothing to do, we need another data point

        new_data = self.parse(record)
        for iface, iface_data in new_data.items():
            if iface not in self.last_data:
                continue  # new iface, no data
            for key in iface_data:
                value = iface_data[key] - self.last_data[iface][key]
                yield (f"net.{iface}.{key}", self.last_ts, now, value)
        self.last_ts = now
        self.last_data = new_data


class CPUStat(StatParser):
    def __init__(self, record):
        self.last_ts = record["ts"]
        self.last_data = self.parse(record)

    def parse(self, record):
        data = {}  # metric -> value
        lines = record["text"].split("\n")
        line_names = ("usage_usec", "user_usec", "system_usec")
        for line in lines:
            if not line:
                continue

            in_columns = line.split()
            assert len(in_columns) == 2, "unexpected line format"

            name, value = in_columns
            if name not in line_names:
                continue

            data[name] = int(value)
        return data

    def process(self, record):
        now = record["ts"]
        if now == self.last_ts:
            return  # nothing to do, we need another data point

        new_data = self.parse(record)
        for key, cpu_data in new_data.items():
            utilization_pct = (
                (cpu_data - self.last_data[key]) / 10**6 / (now - self.last_ts) * 100
            )
            yield (
                f'cpu.{key.replace("usec", "percent")}.cg',
                self.last_ts,
                now,
                utilization_pct,
            )
        self.last_ts = now
        self.last_data = new_data


class DiskStat(StatParser):
    def __init__(self, record):
        self.last_ts = record["ts"]
        self.last_data = self.parse(record)
        self.keys_with_abs_values = set(["io.inprogress"])

    def parse(self, record):
        data = {}
        lines = record["text"].split("\n")[2:]
        column_names = (  # '_major', '_minor', '_device',
            "read.completed",  # count, cumulative
            "read.merged",  # count, cumulative
            "read.sectors",  # count, cumulative
            "read.time",  # ms, cumulative
            "write.completed",  # count, cumulative
            "write.merged",  # count, cumulative
            "write.sectors",  # count, cumulative
            "write.time",  # ms, cumulative
            "io.inprogress",  # count, at the moment
            "io.time",  # ms, cumulative
            "io.time.weighted",  # count, cumulative
            "discard.completed",  # count, cumulative
            "discard.merged",  # count, cumulative
            "discard.sectors",  # count, cumulative
            "discard.time",  # ms, cumulative
            "flush.completed",  # count, cumulative
            "flush.time",  # ms, cumulative
        )
        for line in lines:
            if not line:
                continue
            in_columns = line.split()
            disk_name = in_columns[2]
            assert len(column_names) == len(in_columns) - 3, "unexpected columns"

            num_columns = list(int(val) for val in in_columns[3:])
            data[disk_name] = dict(zip(column_names, num_columns))
        return data

    def process(self, record):
        now = record["ts"]
        if now == self.last_ts:
            return  # nothing to do, we need another data point

        new_data = self.parse(record)
        for disk_name, disk_data in new_data.items():
            if disk_name not in self.last_data:
                continue  # new disk_name, no data
            for key in disk_data:
                if key in self.keys_with_abs_values:
                    new_val = disk_data[key]
                else:  # cumulative metric
                    new_val = disk_data[key] - self.last_data[disk_name][key]
                yield (f"disk.{disk_name}.{key}", self.last_ts, now, new_val)
        self.last_ts = now
        self.last_data = new_data


class DeltaDict(dict):
    def __sub__(self, other):
        assert len(self) == len(other)
        return DeltaDict({key: my_val - other[key] for key, my_val in self.items()})


class ProcStat(StatParser):
    """/proc/stat; cumulative values"""

    def __init__(self, record):
        self.last_ts = record["ts"]
        self.last_data = self.parse(record)

    def parse(self, record):
        data = {}  # metric -> value columns
        lines = record["text"].split("\n")
        # taken from man 5 proc
        cpu_column_names = (
            "user",
            "nice",
            "system",
            "idle",
            "iowait",
            "irq",
            "softirq",
            "steal",
            "guest",
            "guest_nice",
        )
        for line in lines:
            if not line:
                continue

            in_columns = line.split()
            name = in_columns[0]
            if not name.startswith("cpu"):
                continue
            assert len(in_columns) >= 11, "unexpected line format"

            data[name] = DeltaDict(
                {
                    col_name: int(value)
                    for col_name, value in zip(cpu_column_names, in_columns[1:])
                }
            )
        return data

    def process(self, record):
        now = record["ts"]
        if now == self.last_ts:
            return  # nothing to do, we need another data point

        new_data = self.parse(record)
        n_cpus = len(new_data) - 1  # - total
        for cpu_name, cpu_data in new_data.items():
            assert cpu_name in self.last_data, f"CPU {cpu_name} disappeared"
            cpu_deltas = cpu_data - self.last_data[cpu_name]
            cpu_sumtime = sum(cpu_deltas.values())
            for metric, value in cpu_deltas.items():
                percent = value / cpu_sumtime * 100
                if cpu_name == "cpu":
                    # when dealing with aggregate stats, assume 100 % per 1 CPU
                    percent *= n_cpus
                yield (f"{cpu_name}.{metric}_percent", self.last_ts, now, percent)
        self.last_ts = now
        self.last_data = new_data


class BINDJSONStat(StatParser):
    def __init__(self, record):
        self.last_ts = record["ts"]
        self.last_data = self.parse(record)

    @classmethod
    def is_adb(cls, path):
        return (
            len(path) >= 4
            and path[0] == "views"
            and path[2] == "resolver"
            and path[3] == "adb"
        )

    @classmethod
    def is_memory(cls, path):
        return path[0] == "memory" or path[-1] in {
            "HeapMemInUse",
            "HeapMemMax",
            "TreeMemInUse",
            "TreeMemMax",
        }

    @classmethod
    def is_cache_gauge(cls, path):
        return (len(path) >= 5 and path[0] == "views" and path[2] == "resolver") and (
            path[3] == "cache"
            or (
                path[3] == "cachestats"
                and path[4] in {"CacheNodes", "CacheNSECNodes", "CacheBuckets"}
            )
        )

    @classmethod
    def is_zone(cls, path):
        return len(path) == 3 and path[0] == "views" and path[2] == "zones"

    @classmethod
    def yield_from_list_with_name(cls, nested_path, list_value):
        """
        walk list and promote value property "name" to new (virtual) dict key
        """
        for item in list_value:
            yield from cls.yield_numbers(nested_path + (item["name"],), item)

    @classmethod
    def yield_numbers(cls, root_path, in_dict):
        """
        Transform nested dicts and lists into series of tuples. E.g.
        >>> list(BINDJSONStat.yield_numbers((), {'dict': {'nested': {'key': 5}}}))
        [(('dict', 'nested', 'key'), 5)]

        Zone list at ('views', ..., 'zones') are special case and transformed as if the list was dict with key = zone name.
        E.g. (('views', '_default', 'zones', '.', 'serial'), 324)

        Memory contexts are another special case and are transformed in the same way, e.g. "name" property is transformed into virtual dict key.
        Beware that *multiple* memory contexts can have the same name:
        >>> list(BINDJSONStat.yield_numbers((), {"memory":{"InUse":6222,"contexts":[{"id":"0xdeadbeef2040","name":"mctxpoo","inuse":6000},{"id":"0x7f96ef022040","name":"mctxpoo","inuse":222}]}}))
        [(('memory', 'InUse'), 6222), (('memory', 'contexts', '_count'), 2), (('memory', 'contexts', 'mctxpoo', 'inuse'), 6000), (('memory', 'contexts', 'mctxpoo', 'inuse'), 222)]
        """
        for key, value in in_dict.items():
            assert isinstance(key, str), ("unexpected key type", key)
            nested_path = root_path + (key,)
            if isinstance(value, dict):
                yield from cls.yield_numbers(nested_path, value)
            elif isinstance(value, (int, float)):
                yield (nested_path, value)
            elif isinstance(value, list):
                # so far we have only two lists: zones and memory contexts
                # apply special cases for these two
                yield (nested_path + ("_count",), len(value))
                if cls.is_zone(nested_path) or cls.is_memory(nested_path):
                    yield from BINDJSONStat.yield_from_list_with_name(
                        nested_path, value
                    )
            else:
                continue

    @classmethod
    def merge_memory_ctxs(cls, data, new_path, new_value):
        assert len(new_path) > 3, new_path  # ('memory', 'context', 'name', ...)
        ctx_path = new_path[0:3]
        allinone_prefix = new_path[0:2] + ("_allinone",)
        metric_name = new_path[-1]
        for prefix, func, default in [
            ("_min_", min, math.inf),
            ("_max_", max, -math.inf),
            ("_sum_", sum, 0),
        ]:
            suffix = (f"{prefix}{metric_name}",)
            for aggregate_path in [new_path[0:-1] + suffix, allinone_prefix + suffix]:
                data[aggregate_path] = func(
                    (data.get(aggregate_path, default), new_value)
                )
        # let's hope that "references" field is always present
        if new_path == ctx_path + ("references",):
            for count_path in [ctx_path + ("_count",), allinone_prefix + ("_count",)]:
                data[count_path] = data.get(count_path, 0) + 1

    @classmethod
    def is_gauge(cls, metric_path):
        # synthetic
        if metric_path[-1] == "_count":
            return True
        if cls.is_cache_gauge(metric_path):
            return True
        if cls.is_zone(metric_path):
            return metric_path[-1] == "serial"
        if cls.is_memory(metric_path):
            return metric_path[-1] != "total"
        if cls.is_adb(metric_path):
            return True
        if metric_path[0] == "nsstats":
            return metric_path[-1] in ["RecursClients", "TCPConnHighWater"]
        if metric_path[0] == "resstats":
            return metric_path[-1] in ["NumFetch", "QueryCurTCP", "QueryCurUDP"]
        if metric_path[0] == "sockstats":
            return metric_path[-1].endswith("Active")
        return False

    def parse(self, record):
        """
        >>> obj = BINDJSONStat({'text': '{"memory": {"contexts": [{"id": "0xbeef", "name": "mctxpoo", "inuse": 60}, {"id": "0x2040", "name": "mctxpoo", "inuse": 2}]}}', 'ts': 0})
        >>> obj
        <resmon.parsers.BINDJSONStat object ...

        # memory contexts synthetize meta-keys with leading underscore
        >>> newstate = obj.parse({'text': '{"memory": {"contexts": [{"id": "0xbeef", "name": "mctxpoo", "inuse": 6000}, {"id": "0x2040", "name": "mctxpoo", "inuse": 222}, {"id": "0xaaaa", "name": "anotherMctx", "inuse": 778}]}}', 'ts': 1})
        >>> newstate[('memory', 'contexts', '_count')]
        3
        >>> newstate[('memory', 'contexts', 'mctxpoo', '_sum_inuse')]
        6222
        >>> newstate[('memory', 'contexts', 'mctxpoo', '_max_inuse')]
        6000
        >>> newstate[('memory', 'contexts', '_allinone', '_sum_inuse')]
        7000
        >>> newstate[('memory', 'contexts', '_allinone', '_max_inuse')]
        6000
        >>> newstate[('memory', 'contexts', '_allinone', '_min_inuse')]
        222

        # normal counters
        >>> newstate = obj.parse({'text': '{"opcodes": {"QUERY": 80}}', 'ts': 2})
        >>> newstate[('opcodes', 'QUERY')]
        80
        """
        under_construction = {}
        for path, value in self.yield_numbers((), json.loads(record["text"])):
            if path[0:2] != ("memory", "contexts") or path == (
                "memory",
                "contexts",
                "_count",
            ):
                assert path not in under_construction, ("duplicite key", path)
                under_construction[path] = value
            else:
                BINDJSONStat.merge_memory_ctxs(under_construction, path, value)
        return under_construction

    def process(self, record):
        """
        >>> obj = BINDJSONStat({'text': '{"opcodes": {"QUERY": 80}, "nsstats": {"RecursClients": 1}}', 'ts': 1})
        >>> obj
        <resmon.parsers.BINDJSONStat object ...

        # opcodes are counter, RecursClients is gauge - counters should return diff
        >>> list(obj.process({'text': '{"opcodes": {"QUERY": 100}, "nsstats": {"RecursClients": 5}}', 'ts': 2}))
        [('bindstats.opcodes.QUERY', 1, 2, 20), ('bindstats.nsstats.RecursClients', 2, 2, 5)]
        """
        now = record["ts"]
        if now == self.last_ts:
            return  # nothing to do, we need another data point

        new_data = self.parse(record)
        for key, new_value in new_data.items():
            if key not in self.last_data:
                continue
            if self.is_gauge(key):
                value = new_value
                from_ts = now
            else:
                old_value = self.last_data[key]
                value = new_value - old_value
                from_ts = self.last_ts
            key_dotted = ".".join(("bindstats",) + key)
            yield (key_dotted, from_ts, now, value)
        self.last_ts = now
        self.last_data = new_data
