import re


def cgroup_stat_to_chart_id(cgroup: str, statname: str):
    cgroup = re.sub("docker-.*scope", "docker", cgroup)
    cgroup = re.sub("libpod-.*scope", "docker", cgroup)
    return f"{statname}-{cgroup}"
