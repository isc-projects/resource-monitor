# Resource Monitor

The following scripts are used to collect resource use statistics, parse and
plot them.

Tested with Python 3.10+. Minimum Python version is probably 3.8+?

## monitor.py

Periodically captures the current values of watched system/cgroup paths.

If some statistics are missing and the script throws an exception, it might be
necessary to activate these statistics using `cgroup.subtree_control`. These
need to be applied from the top of hierarchy all the way to the bottom where the
values are collected, e.g:

```
# echo "+cpu +memory +io +pids" > /sys/fs/cgroup/cgroup.subtree_control
# echo "+cpu +memory +io +pids" > /sys/fs/cgroup/system.slice/cgroup.subtree_control
```

See
https://git.kernel.org/pub/scm/linux/kernel/git/tj/cgroup.git/tree/Documentation/admin-guide/cgroup-v2.rst?id=3d7285a335edaf23b699e87c528cf0b0070e3293#n383
for more information.

## parse.py

Parse the relevant data from the raw capture and output a JSON file.

## plot.py

Use together with DNS Shotgun results directory in order to plot the parsed data
in the proper time frame (for the duration of the test run).
