#!/usr/bin/python3

import argparse
import collections
import os
import urllib.parse

# try to find faster JSON libraries
try:
    import ujson as json
except ImportError:
    try:
        import rapidjson as json  # type: ignore
    except ImportError:
        import json  # type: ignore
import logging
from pathlib import Path
import sys
from typing import Dict, List, Tuple, Type, Union

# pylint: disable=wrong-import-order,wrong-import-position
sys.path.insert(0, str(Path(__file__).resolve().parent))
from resmon import constants, config, parsers, util

# pylint: enable=wrong-import-order,wrong-import-position


def load_json(infile):
    """parse newline separated stream of JSON objects"""
    for line in infile:
        if line:
            yield json.loads(line)


def get_parsers(conf) -> Dict[Union[str, Path], Type[parsers.StatParser]]:
    path_parsers = {}

    def add_parser(key: Union[str, Path], parser_name: str):
        try:
            parser = getattr(parsers, parser_name)
        except AttributeError:
            logging.warning("unknown parser: %s", parser_name)
        else:
            path_parsers[key] = parser

    def find_parser(where, key_name, key_transform):
        for item in where:
            try:
                key = item[key_name]
                parser_name = item["parser"]
            except KeyError:
                continue
            add_parser(key_transform(key), parser_name)

    find_parser(conf.get("systemwide_paths", []), "path", Path)
    find_parser(conf.get("cgroup_files", []), "name", str)
    find_parser(conf.get("http_urls", {}), "url", str)
    return path_parsers


def parse_all(infile, path_parsers: Dict[Union[str, Path], Type[parsers.StatParser]]):
    """transform input stream of JSON objects to dictionary:
    [cgroup name][metric name] = [(timestamp from, timestamp to, metric value)]
    """
    warned_about = set()
    records = sorted(load_json(infile), key=lambda rec: rec["ts"])
    state = {}  # stateful parsers for individual paths; type: Dict[str, Any]

    # output: collection name -> stat name -> [ts from, to, value]
    stats = collections.defaultdict(
        lambda: collections.defaultdict(list)
    )  # type: Dict[str, Dict[str, List[Tuple[float, float, float]]]]
    for rec in records:
        try:
            path = Path(rec["path"])
            output_group = path.parent.name
        except KeyError:
            path = rec["http_url"]
            output_group = urllib.parse.quote(str(path), safe="")
        logging.debug("%f %s", rec["ts"], path)
        try:
            parser = path_parsers[path]
        except KeyError:
            try:
                parser = path_parsers[path.name]
            except (AttributeError, KeyError):
                if path not in warned_about:
                    logging.warning("skipping unsupported key %s", path)
                    warned_about.add(path)
                continue

        if path not in state:  # first time, parser init
            state[path] = parser(rec)

        for stat_name, ts_from, ts_to, value in state[path].process(rec):
            stats[output_group][stat_name].append((ts_from, ts_to, value))

    return stats


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    conf_action = parser.add_argument(
        "-c",
        "--config",
        type=Path,
        default=constants.RESMON_CONFIG_PATH,
        action=config.ConfigAction,
        help="configuration file",
    )
    parser.add_argument(
        "-i",
        "--input",
        type=Path,
        default=constants.RESMON_CAPTURED_FILENAME,
        help="input captured resmon JSON file",
    )
    parser.add_argument(
        "-o",
        "--output",
        type=Path,
        default=constants.RESMON_PARSED_DIRECTORY,
        help="output directory for parsed resmon JSON files",
    )
    args = parser.parse_args()
    if args.config == constants.RESMON_CONFIG_PATH:
        # invoke action that parses config manually (it is not applied to default=)
        conf_action(parser, args, conf_action.default, "-c/--config")
    return args


def main():
    logging.basicConfig(level=logging.INFO)
    args = parse_args()
    path_parsers = get_parsers(args.config)

    with open(args.input, encoding="utf-8") as infile:
        stats = parse_all(infile, path_parsers)
    if not os.path.exists(args.output):
        os.makedirs(args.output)
    for cgroup, cgroup_data in stats.items():
        for stat, data in cgroup_data.items():
            chart_id = util.cgroup_stat_to_chart_id(cgroup, stat)
            path = Path(args.output) / f"{chart_id}.json"
            data = {cgroup: {stat: data}}
            with open(path, "w", encoding="utf-8") as statfile:
                json.dump(data, statfile)


if __name__ == "__main__":
    main()
