#!/usr/bin/python3
"""
Periodically dump content of selected /proc and /sys files as a stream of
JSON-formatted lines. No data post-processing is done in this script.
Requires cgroups v2.

SPDX-FileCopyrightText: Internet Systems Consortium, Inc. ("ISC")
SPDX-License-Identifier: MPL-2.0
"""

import argparse
import asyncio
import json
import logging
from pathlib import Path
import signal
import sys
import time
from typing import List, Optional, Union, Tuple

# pylint: disable=wrong-import-order,wrong-import-position
sys.path.insert(0, str(Path(__file__).resolve().parent))
from resmon import constants, config

# pylint: enable=wrong-import-order,wrong-import-position

# globals for signal handler
RUNNING = True
PRODUCERS = []  # type: List[asyncio.Task]


def sigint(_signum, _frame):
    """
    Cancel producers but keep consumer running so no information from queue is
    lost
    """
    global PRODUCERS  # pylint: disable=global-variable-not-assigned
    global RUNNING

    RUNNING = False
    for task in PRODUCERS:
        task.cancel()


async def read_to_json(fileobj) -> str:
    """Return JSON string with file path, complete content of file, and timestamp."""
    now = time.time()
    text = fileobj.read()
    result = {"ts": now, "path": fileobj.name, "text": text}
    return json.dumps(result)


async def watch_file(
    samples_q: asyncio.Queue, file_path: Path, interval: float
) -> None:
    """Sample file and put log records into queue."""
    try:
        with open(file_path, encoding="utf-8") as fileobj:
            try:
                while True:
                    record = await read_to_json(fileobj)
                    fileobj.seek(0)
                    await samples_q.put(record)
                    await asyncio.sleep(interval)
            except asyncio.exceptions.CancelledError:
                return
            except OSError as ex:
                logging.critical(
                    "file %s went away, terminating (%s)", fileobj.name, ex
                )
                sigint(None, None)
                return
    except FileNotFoundError as ex:
        logging.critical(
            "file %s not found, terminating (%s)\n\n"
            "If the file above is a cgroup file, refer to README for possible solution",
            file_path,
            ex,
        )
        sigint(None, None)
        return


async def get_to_json(client, url) -> str:
    """Return JSON string with URL, complete (decoded) text content, and timestamp."""
    now = time.time()
    req = await client.request("GET", url)
    result = {"ts": now, "http_url": url, "text": req.content.decode("utf-8")}
    return json.dumps(result)


async def watch_http_url(samples_q: asyncio.Queue, url: str, interval: float) -> None:
    """Sample HTTP URL and put log records into queue. Requires httpcore library."""
    import httpcore  # pylint: disable=import-outside-toplevel

    async with httpcore.AsyncConnectionPool() as client:
        try:
            while True:
                try:
                    record = await get_to_json(client, url)
                    await samples_q.put(record)
                except (httpcore.NetworkError, httpcore.RemoteProtocolError) as ex:
                    logging.error("failed to fetch URL %s: %s", url, ex)
                await asyncio.sleep(interval)
        except asyncio.exceptions.CancelledError:
            return


async def write_queue(output_fobj, samples_q: asyncio.Queue) -> None:
    """Write individual lines from samples_q into output file"""
    while True:
        record = await samples_q.get()
        print(record, file=output_fobj)
        samples_q.task_done()


async def watch_and_write(
    output: Optional[str],
    paths: List[Tuple[Path, float]],
    http_urls: List[Tuple[str, float]],
):
    """Watch all specified files in paralell and periodically dump them into output."""
    samples_q = asyncio.Queue()  # type: asyncio.Queue

    global PRODUCERS  # signal handler, pylint: disable=global-statement
    PRODUCERS = [
        asyncio.create_task(watch_file(samples_q, path, interval))
        for path, interval in paths
    ]
    PRODUCERS.extend(
        asyncio.create_task(watch_http_url(samples_q, url, interval))
        for url, interval in http_urls
    )
    if output is None:
        output_fobj = sys.stdout
    else:
        # pylint: disable=consider-using-with
        output_fobj = open(output, "w", encoding="utf-8")
    consumer = asyncio.create_task(write_queue(output_fobj, samples_q))

    try:
        await asyncio.gather(*PRODUCERS)
    except asyncio.exceptions.CancelledError:
        pass

    await samples_q.join()  # implicitly awaits consumer
    consumer.cancel()


def get_cgroup_paths(
    base: Union[str, Path], cgroup_filenames: List[Tuple[str, float]], glob=None
) -> List[Tuple[Path, float]]:
    """
    Generate paths to statistics files for cgroups matching glob.
    glob=None generates paths for the base cgroup.
    """
    base_path = Path(base)
    if not base_path.is_dir():
        raise ValueError(f"base cgroup path {base} must be a directory")

    if glob:
        cgrps = list(path for path in base_path.glob(glob) if path.is_dir())
        if len(cgrps) == 0:
            raise ValueError(f"no cgroups found with base dir {base} glob {glob}")
    else:
        cgrps = [base_path]

    cgroup_paths = []
    for cgrp in cgrps:
        for filename, interval in cgroup_filenames:
            cgroup_paths.append((cgrp / filename, interval))
    return cgroup_paths


def wait_for_files(
    cgroup_base_dir: Path,
    systemwide_paths: List[Tuple[Path, float]],
    cgroup_filenames: List[Tuple[str, float]],
    cgroup_glob: Optional[str],
) -> List[Tuple[Path, float]]:
    """
    Wait until at least one cgroup matching specified glob exists, and return
    all paths to statistical files and the intervals to watch them.
    """
    paths = list(systemwide_paths)
    while RUNNING:
        try:
            cgroup_paths = get_cgroup_paths(
                cgroup_base_dir, cgroup_filenames, cgroup_glob
            )
        except ValueError as ex:
            logging.info("waiting: %s", ex)
            time.sleep(0.1)
        else:
            return paths + cgroup_paths
    return paths  # fallback return to cover all branches of code


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    conf_action = parser.add_argument(
        "-c",
        "--config",
        type=Path,
        default=constants.RESMON_CONFIG_PATH,
        action=config.ConfigAction,
        help="configuration file",
    )
    parser.add_argument(
        "--cgroup-base-dir",
        type=Path,
        default=constants.CGROUP_BASE_DIR,
        help="base cgroup path, glob is applied here",
    )
    parser.add_argument(
        "--cgroup-glob",
        type=str,
        default=None,
        help="glob for sub-cgroups to monitor, e.g. docker-*.scope",
    )
    parser.add_argument(
        "-o",
        "--output",
        type=Path,
        default=constants.RESMON_CAPTURED_FILENAME,
        help="output captured resmon JSON file",
    )
    args = parser.parse_args()
    if args.config == constants.RESMON_CONFIG_PATH:
        # invoke action that parses config manually (it is not applied to default=)
        conf_action(parser, args, conf_action.default, "-c/--config")
    return args


def main():
    logging.basicConfig(level=logging.INFO, format="%(levelname)s  %(message)s")
    signal.signal(signal.SIGINT, sigint)

    args = parse_args()
    systemwide_paths = [
        (Path(item["path"]), item["interval"])
        for item in args.config.get("systemwide_paths", [])
    ]
    cgroup_filenames = [
        (item["name"], item["interval"]) for item in args.config.get("cgroup_files", [])
    ]
    paths = wait_for_files(
        args.cgroup_base_dir, systemwide_paths, cgroup_filenames, args.cgroup_glob
    )
    http_urls = [
        (item["url"], item["interval"]) for item in args.config.get("http_urls", {})
    ]

    if RUNNING:
        logging.info("gathering content of %s, %s", paths, http_urls)
        asyncio.run(watch_and_write(args.output, paths, http_urls))


if __name__ == "__main__":
    main()
